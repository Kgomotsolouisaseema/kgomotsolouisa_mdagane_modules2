void main() {
  //Declaring and initialising variables
  List mtn_award_winningApps = [
    "FNB-2012",
    "SnapScan-2013",
    "LiveInsepct-2014",
    "Wumdrop-2015",
    "Domestly-2016",
    "Shyft-2017",
    "Khula ecosystem-2018",
    "Naked insurance-2019",
    "EasyEquities-2020",
    "Ambani Africa-2021",
  ];

  //Sort the award winning App in alphabetical order
  mtn_award_winningApps.sort();

  //Print the names of the apps in Alphabetical order
  print(
      "Award Winning Apps (in alphabetical order) are $mtn_award_winningApps");

  //Print the winning app of 2017 and winning app of 2018
  print("Winning app of 2017 is :");
  print(mtn_award_winningApps[7]);
  print("The winning app of 2018 is :");
  print(mtn_award_winningApps[4]);

  //print total number of apps
  print("The total number of Winning apps is :");
  print(mtn_award_winningApps.length);
}
